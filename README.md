# Codigo del curso Docker - Guía práctica de uso para desarrolladores
# Instructor
> [Andrés Guzmán](https://www.udemy.com/course/docker-guia-practica/) 
 

# Udemy
* Docker - Guía práctica de uso para desarrolladores

## Contiene

* Docker CLI
* Construcción de imágenes
* Uso de imágenes, redes, volúmenes
* Repositorios privados y despliegues
* Dockerfile y multi-stage build
* Docker compose - multi-container applications
* Deployments Digital Ocean
* GitHub Actions - Automatizar construcción
 

## Descripción

* Docker es una plataforma de software de código abierto para automatizar despliegues de aplicaciones dentro de contenedores, y proporciona una forma eficaz de controlar versiones, agilizar el desarrollo y es una pieza fundamental para Kubernetes. También cuenta con una sección introductoria a Kubernetes (K8s).

* Poder crear, usar, y desplegar imágenes es una habilidad necesaria para cualquier desarrollador o administrador de hoy en día, ahorra mucho tiempo en la preparación de diferentes ambientes de desarrollo, testing, staging y production.  
   

---

## Notas
 
 


> [Herramientas](https://gist.github.com/Klerith/5bd148b2e16325752219b68995ddf91b) 
> [Codigo](https://github.com/Klerith/clean-course/tree/main) 

~~~
* Repositorio solo en ** Gitlab  **

 

---
#### Version  V.0.1 
* **V.0.1**  _>>>_  Iniciando proyecto [ 01 NOV, 2023 ]  
 
 