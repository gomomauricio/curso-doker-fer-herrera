#    Bases de Docker


## Hola Mundo
* Imagen - Es un archivo construido por capas, que contiene todas las dependencias para ejecutarse

* En terminal INSTALAR:
* * En terminal docker pull hello-world

* EJECUTAR [correr un contenedor] :
* * docker container run hello-world

* MOSTRAR
* * docker container ls -a

* DETENER container
* * docker container stop c28aaca853db  (se puede usar los 3 primeros digitos del id)

* BORRAR container
* * docker container rm c28aaca853db (se puede concatenar dejando un espacio entre cada id)

* IMAGEN TUTORIAL  --primer puerto es del equipo, el segundo es del contenedor
* * docker container run -d -p 80:80 docker/getting-started
* * docker container run -d -p 8080:80 docker/getting-started
 
## Variables de entorno

* Postgres
* Iniciar nuevo
* * docker container run --name mgm-postgres -dp 5432:5432 -e POSTGRES_PASSWORD=mysecretp postgres

* Reiniciar
* * docker container run -dp 5432:5432 -e POSTGRES_PASSWORD=mysecretp postgres

* LOGS
* * docker container logs -f [id]

